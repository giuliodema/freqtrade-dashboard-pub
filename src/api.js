$(document).ready(function() {
    const base_path = configVars.base_path;
    const username = configVars.username;
    const password = configVars.password;


    var access_token;
    var refresh_token;
    var currenct_pct;
    var xValue = 0;
    var dataPoints = [];
    var dataPointTotOpenTrades = []
    // Authentication
    function main(){
      $.ajax({
          headers: {
              "Authorization": "Basic " + btoa(username + ":" + password)
          },
          method: "POST",
          url: base_path + "/token/login",
          // dataType: 'json',
          data: {},
          success: function (){}
      }).then(function(data) {
        access_token = data.access_token;
        refresh_token = data.refresh_token;
        $('.token-id').append(access_token);
        
        // GET Open Trades
        $.ajax({
          url: base_path + "/status",
          headers: {
              "Authorization": "Bearer " + access_token
          },
          method: "GET",
          contentType: "application/json",
          dataType: 'json',
          data: {},
        }).then(function(data) {
          $("#open-trades tbody").empty()
          xValue++;
          current_profit = 0
          $(data).each(function(){ 

            current_profit = current_profit + this.profit_abs

            if (!(this.trade_id in dataPoints)) {
              dataPoints[this.trade_id] = []
            }
            if (this.profit_pct >= 0)  class_str = "success"
            else class_str = "danger"

            currenct_pct = this.profit_pct
            $("#open-trades tbody").append(
              '<tr class="'+ class_str +'"><td>' + this.trade_id + '</td><td><a href="https://www.binance.com/en/trade/' + this.pair.replace("/", "_") + '?layout=pro" target="_blank">' 
              + this.pair + '</a></td><td>' 
              + this.open_date + '</td><td>' 
              + this.open_date_hum + '</td><td>' 
              + parseFloat(this.stake_amount).toFixed(2) + '</td><td>' 
              + this.open_rate + '</td><td>' 
              + this.current_rate + '</td><td>' 
              + this.stoploss_current_dist_pct + ' %</td><td>'
              + parseFloat(this.profit_abs).toFixed(2) + '</td><td>'
              + parseFloat(this.profit_pct).toFixed(2) + ' %</td><td>' 
              + '<div id="chartContainer' + this.trade_id + '" style="height: 80px; width: 100%;"></div>' + '</td></tr>'
            )

            // pair graph creation
            var graph_options = {
              theme: "light2",
              data: [{
                type: "spline",
                dataPoints: dataPoints[this.trade_id ]
              }]
            };
      
            $("#chartContainer" + this.trade_id).CanvasJSChart(graph_options);
      
            //inject data in pairs graph
            dataPoints[this.trade_id].push({ x: xValue, y: currenct_pct });
            //graph will be max 30 points plotted
            if (dataPoints[this.trade_id].length > 60) dataPoints[this.trade_id].shift();
            $("#chartContainer" + this.trade_id).CanvasJSChart().render();

            


           

          });
           // open trades profit graph creation
           var graph_options = {
            theme: "light2",
            data: [{
              type: "spline",
              dataPoints: dataPointTotOpenTrades
            }]
          };
          console.log(dataPointTotOpenTrades)
    
          $("#chartContainerOpenProfit").CanvasJSChart(graph_options);
          //inject data in open trades profit graph
          dataPointTotOpenTrades.push({ x: xValue, y: current_profit});
          //graph will be max 30 points plotted
          if (dataPointTotOpenTrades.length > 60) dataPointTotOpenTrades.shift();
          $("#chartContainerOpenProfit").CanvasJSChart().render();
          $("#total-profit").empty().append(parseFloat(current_profit).toFixed(2))
          document.title = parseFloat(current_profit).toFixed(2)

        });

        // GET Trade History
        $.ajax({
          url: base_path + "/trades?limit=20",
          headers: {
              "Authorization": "Bearer " + access_token
          },
          method: "GET",
          contentType: "application/json",
          dataType: 'json',
          data: {},
        }).then(function(data) {
          $("#trade-history tbody").empty()
          current_last_profit = 0
          $(data.trades.sort(custom_sort)).each(function(){ 


            // delete useless graphs data in order to save memory
            delete dataPoints[this.trade_id];

            current_last_profit = current_last_profit + this.profit_abs

            if (this.profit_pct >= 0)  class_str = "success"
            else class_str = "danger"

            $("#trade-history tbody").append(
              '<tr class="'+ class_str +'"><td>' + this.trade_id + '</td><td><a href="https://www.binance.com/en/trade/' + this.pair.replace("/", "_") + '?layout=pro" target="_blank">' 
              + this.pair + '</a></td><td>' 
              + this.open_date + '</td><td>' 
              + this.close_date + '</td><td>' 
              + this.close_date_hum + '</td><td>' 
              + this.open_rate + '</td><td>' 
              + this.close_rate + '</td><td>' 
              + this.sell_reason + '</td><td>' 
              + this.sell_order_status + '</td><td>' 
              + parseFloat(this.stake_amount).toFixed(2) + '</td><td>' 
              + parseFloat(this.profit_pct).toFixed(2) + ' %</td><td>' 
              + parseFloat(this.profit_abs).toFixed(2) + '</td></tr>'
            )
          });

          $("#total-last-profit").empty().append(parseFloat(current_last_profit).toFixed(2))
        });

        // GET Daily
        $.ajax({
          url: base_path + "/daily",
          headers: {
              "Authorization": "Bearer " + access_token
          },
          method: "GET",
          contentType: "application/json",
          dataType: 'json',
          data: {},
        }).then(function(data) {
          $("#daily-stats tbody").empty()
          $(data.data).each(function(){ 
            if (this.abs_profit > 0)  class_str = "success"
            else if (this.abs_profit < 0) class_str = "danger"
            else class_str = "warning"
            $("#daily-stats tbody").append(
              '<tr class="'+ class_str +'"><td>' + this.date + '</td><td>' 
              + this.abs_profit + '</td><td>' 
              + this.trade_count + '</td></tr>'
            )
          });
          $("#update-time").empty().append(Date($.now()))
        });

      });

      

     
      function custom_sort(a, b) {
        return new Date(b.close_date).getTime() - new Date(a.close_date).getTime();
      }




    } 
    main();
    setInterval(main, 30000);  

});