# Usage

Copy config.js.example to config.js and edit with your API params.

Then open index.html in your browser.

# Preview

![alt text](img/preview.PNG "Dashboad Preview")
